describe('Cell -', function(){
    'use strict';
    describe('when creating', function () {
        it('should initialize with passed in x position', function () {
            var xPosition = 3;

            var cell = createCell(xPosition);
            expect(cell.x).toEqual(xPosition);
        });
        it('should initialize with passed in y position', function(){
            var yPosition = 9;

            var cell = createCell(1,yPosition);
            expect(cell.y).toEqual(yPosition);
        });
        it('should calculate its neighbors based on its position', function () {
            var cell = createCell(3,9);

            expect(cell.neighboringPositions).toEqual([
                {x:2,y:8},
                {x:2,y:9},
                {x:2,y:10},
                {x:3,y:8},
                {x:3,y:10},
                {x:4,y:8},
                {x:4,y:9},
                {x:4,y:10}
            ]);

        });
    });
    describe('when generation is advanced', function () {
        beforeEach(function () {
            this.fakePopulation = {};
            this.fakePopulation.removeCell = jasmine.createSpy();
            this.cell = createCell(2,2);
            this.fakeActions = {
                cellDies: {name:'cellDies'},
                cellLives: {name:'cellLives'}
            };
            spyOn(populationActionFactory,'cellDies').and.returnValue(this.fakeActions.cellDies);
            spyOn(populationActionFactory,'cellLives').and.returnValue(this.fakeActions.cellLives);
        });
        describe('and it is underpopulated', function () {
            it('should die with no neighbors', function () {
                this.fakePopulation.countCellsAtPositions = jasmine.createSpy().and.returnValue(0);

                var action = this.cell.advanceGeneration(this.fakePopulation);
                expect(populationActionFactory.cellDies).toHaveBeenCalledWith(this.cell);
                expect(action).toBe(this.fakeActions.cellDies);
            });
            it('should die with one live neighbor', function () {
                this.fakePopulation.countCellsAtPositions = jasmine.createSpy().and.returnValue(1);

                var action = this.cell.advanceGeneration(this.fakePopulation);
                expect(populationActionFactory.cellDies).toHaveBeenCalledWith(this.cell);
                expect(action).toBe(this.fakeActions.cellDies);
            });
        });
        describe('and it is stable', function () {
            it('with two live neighbors', function () {
                this.fakePopulation.countCellsAtPositions = jasmine.createSpy().and.returnValue(2);

                var action = this.cell.advanceGeneration(this.fakePopulation);
                expect(populationActionFactory.cellLives).toHaveBeenCalledWith(this.cell);
                expect(action).toBe(this.fakeActions.cellLives);
            });
            it('with three live neighbors', function () {
                this.fakePopulation.countCellsAtPositions = jasmine.createSpy().and.returnValue(3);

                var action = this.cell.advanceGeneration(this.fakePopulation);
                expect(populationActionFactory.cellLives).toHaveBeenCalledWith(this.cell);
                expect(action).toBe(this.fakeActions.cellLives);
            });
        });

        describe('and it is overpopulated', function () {
            it('should die with 4 live neighbors', function () {
                this.fakePopulation.countCellsAtPositions = jasmine.createSpy().and.returnValue(4);

                var action = this.cell.advanceGeneration(this.fakePopulation);
                expect(populationActionFactory.cellDies).toHaveBeenCalledWith(this.cell);
                expect(action).toBe(this.fakeActions.cellDies);
            });
            it('should die with 8 live neighbors', function () {
                this.fakePopulation.countCellsAtPositions = jasmine.createSpy().and.returnValue(8);

                var action = this.cell.advanceGeneration(this.fakePopulation);
                expect(populationActionFactory.cellDies).toHaveBeenCalledWith(this.cell);
                expect(action).toBe(this.fakeActions.cellDies);
            });
        });
    });
});