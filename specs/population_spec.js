describe('Population - ',function () {
    'use strict';
    describe('when initializing population', function () {
        it('should contain no cells', function () {
            var population = createPopulation();
            expect(population.getAllCells().length).toEqual(0);
        });
    });
    describe('when adding a cell to population', function () {
        describe('and the cell does not exist at that position', function () {
            it('should be added to the population', function () {
                var population = createPopulation();
                var fakeCell = {x:1,y:2,fakeCell:'fakeCell'};
                population.addCell(fakeCell);
                var allCells = population.getAllCells();
                expect(allCells.length).toEqual(1);
                expect(allCells[0]).toEqual(fakeCell);
            });
        });
        describe('and a cell already exists at that position', function () {
            it('should not alter the existing cell',function () {
                var population = createPopulation();
                var existingCell = {x:3,y:6,fakeCell:'fakeCell'};
                addCellsToPopulation(population,[{x:1,y:9},existingCell,{x:8,y:1}]);
                var newCell = {x:existingCell.x, y:existingCell.y,fakeCell:'newCell'};
                population.addCell(newCell);
                var actualCell = population.getCellByPosition(existingCell.x, existingCell.y);
                expect(actualCell).toBe(existingCell);
            });
        });
    });
    describe('when getting cell by position', function () {
        describe('and the cell is present', function () {
            it('should return the matching cell', function () {
                var population = createPopulation();
                var cellToFind = {x:3,y:6,fakeCell:'fakeCell'};
                addCellsToPopulation(population,[{x:1,y:9},cellToFind,{x:8,y:1}]);

                var matchedCell = population.getCellByPosition(cellToFind.x,cellToFind.y);
                expect(matchedCell).toBe(cellToFind);
            });
        });
        describe('and the cell is not in population', function () {
            it('should return undefined', function () {
                var population = createPopulation();
                var cellToFind = {x:3,y:6,fakeCell:'fakeCell'};
                addCellsToPopulation(population,[{x:1,y:9},{x:8,y:1}]);
                var matchedCell = population.getCellByPosition(cellToFind.x,cellToFind.y);
                expect(matchedCell).toBeUndefined();
            });
        });
    });
    describe('when removing cell', function () {
        describe('and the cell is in the population', function () {
            it('should remove the cell from the population', function () {
                var population = createPopulation();
                var cellToRemove = {x:3,y:6,fakeCell:'fakeCell'};
                addCellsToPopulation(population,[{x:1,y:9},cellToRemove,{x:8,y:1}]);
                population.removeCell(cellToRemove);

                var allCells = population.getAllCells();
                expect(allCells.length).toEqual(2);
                expect(population.getCellByPosition(cellToRemove.x,cellToRemove.y)).toBeUndefined();
            });
        });
    });
    describe('when counting cells from array of positions', function () {
        describe('and there are matching positions', function () {
            it('should return the number of matches', function () {
                var population = createPopulation();
                var matches = [{x:3,y:7},{x:5,y:9},{x:1,y:5}];
                addCellsToPopulation(population,[{x:1,y:9},matches[0],{x:8,y:1},matches[1],matches[2]]);

                var matchCount = population.countCellsAtPositions(matches);
                expect(matchCount).toEqual(matches.length);
            });
        });
    });
    describe('when advancing generation', function () {
        beforeEach(function () {
            this.fakeAction = {
                executeAction: jasmine.createSpy()
            };

        });
        var createFakeCell = function (x, y, context) {
            return {
                x:x, y:y,
                neighboringPositions:[],
                advanceGeneration: jasmine.createSpy().and.returnValue(context.fakeAction)
            };
        };

        it('should advance generation of each cell in the population', function () {
            var population = createPopulation();
            var cells = [
                    createFakeCell(2,4,this),
                    createFakeCell(4,4,this)
                ];
            addCellsToPopulation(population,cells);

            population.advanceGeneration();
            expect(cells[0].advanceGeneration).toHaveBeenCalledWith(population);
            expect(cells[1].advanceGeneration).toHaveBeenCalledWith(population);
            expect(this.fakeAction.executeAction).toHaveBeenCalledWith(population);
            expect(this.fakeAction.executeAction.calls.count()).toEqual(cells.length);

        });
        it('should add a birth action for each empty tile with exactly 3 neighbors', function () {
             var population = createPopulation();
             addCellsToPopulation(population,[
                 createCell(2,4),
                 createCell(2,2),
                 createCell(4,4),
                 createCell(4,5),
                 createCell(4,6),
                 createCell(9,9)
             ]);
             var fakeBirthAction = {
                 executeAction: jasmine.createSpy()
             };
             spyOn(populationActionFactory,'cellBorn').and.returnValue(fakeBirthAction);

             population.advanceGeneration();
             expect(populationActionFactory.cellBorn).toHaveBeenCalledWith(3,3);
             expect(populationActionFactory.cellBorn).toHaveBeenCalledWith(3,4);
             expect(populationActionFactory.cellBorn).toHaveBeenCalledWith(5,5);
             expect(fakeBirthAction.executeAction).toHaveBeenCalledWith(population);
             expect(fakeBirthAction.executeAction.calls.count()).toEqual(3);
        });
    });


    var addCellsToPopulation = function (population, cellsToAdd) {
        cellsToAdd.forEach(function (cell) {
            population.addCell(cell);
        });
    };
});