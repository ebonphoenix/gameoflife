describe('Population Action -', function () {
    'use strict';
    describe('die action', function () {
        it('should remove cell from population', function () {
            var fakeCell = {x:2, y:4};
            var fakePopulation = {
                removeCell: jasmine.createSpy()
            };

            var action = populationActionFactory.cellDies(fakeCell);
            action.executeAction(fakePopulation);
            expect(fakePopulation.removeCell).toHaveBeenCalledWith(fakeCell);
        });
    });
    describe('live action', function () {
        it('should allow execution but have no effect', function () {
            var fakeCell = {x:2, y:4};
            var fakePopulation = {
                removeCell: jasmine.createSpy()
            };

            var action = populationActionFactory.cellLives(fakeCell);
            action.executeAction(fakePopulation);
            expect(fakePopulation.removeCell).not.toHaveBeenCalledWith(fakeCell);
        });
    });
    describe('born action',function () {
        it('should add a newly created cell to the population', function () {
            var fakeCell = {x:2, y:4};
            spyOn(window,'createCell').and.returnValue(fakeCell);
            var fakePopulation = {
                addCell: jasmine.createSpy()
            };

            var action = populationActionFactory.cellBorn(fakeCell.x, fakeCell.y);
            action.executeAction(fakePopulation);
            expect(window.createCell).toHaveBeenCalledWith(fakeCell.x, fakeCell.y);
            expect(fakePopulation.addCell).toHaveBeenCalledWith(fakeCell);
        });
    });
});