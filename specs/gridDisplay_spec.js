describe('Grid Display -', function(){
    'use strict';
    beforeAll(function () {
        this.elementIds = {
            canvasId:'canvasId',
            columnsInputId:'columnsInputId',
            rowsInputId:'rowsInputId',
            cellSizeInputId:'cellSizeInputId',
            runButtonId:'runButtonId',
            maxGenerationsId:'maxGenerationsId'
        };
    });
    describe('when initializing', function () {
        beforeAll(function () {
            spyOn(document,'getElementById').and.callFake(function (id) {
                return {
                    name: id,
                    addEventListener: jasmine.createSpy()
                };
            });
            this.populationFake = {name:'fakePopulation'};
            spyOn(window,'createPopulation').and.returnValue(this.populationFake);

            this.gridDisplay = getGridDisplay(this.elementIds);

        });
        it('should store a reference to the canvas and controls', function () {
            Object.values(this.elementIds).forEach(function (id) {
                expect(document.getElementById).toHaveBeenCalledWith(id);
            });

            expect(this.gridDisplay.canvas.name).toEqual(this.elementIds.canvasId);
            expect(this.gridDisplay.inputs.columns.name).toEqual(this.elementIds.columnsInputId);
            expect(this.gridDisplay.inputs.rows.name).toEqual(this.elementIds.rowsInputId);
            expect(this.gridDisplay.inputs.cellSize.name).toEqual(this.elementIds.cellSizeInputId);
            expect(this.gridDisplay.inputs.maxGenerations.name).toEqual(this.elementIds.maxGenerationsId);
            expect(this.gridDisplay.actions.run.name).toEqual(this.elementIds.runButtonId);
        });
        it('should create and store a new population collection', function () {
            expect( this.gridDisplay.population).toEqual(this.populationFake);
        });
        it('should update canvas size when any input changes', function () {
            expect(this.gridDisplay.inputs.columns.addEventListener).toHaveBeenCalledWith('input',this.gridDisplay.updateCanvasSize);
            expect(this.gridDisplay.inputs.rows.addEventListener).toHaveBeenCalledWith('input',this.gridDisplay.updateCanvasSize);
            expect(this.gridDisplay.inputs.cellSize.addEventListener).toHaveBeenCalledWith('input',this.gridDisplay.updateCanvasSize);
        });
        it('should perform an action when mouse is clicked on canvas', function () {
            expect(this.gridDisplay.canvas.addEventListener).toHaveBeenCalledWith('mousedown',jasmine.any(Function));
        });
        it('should kick off the generations run when run button is clicked', function () {
            expect(this.gridDisplay.actions.run.addEventListener).toHaveBeenCalledWith('click',this.gridDisplay.runGenerations);
        });
    });
    describe('when updating canvas size', function () {
        beforeEach(function () {
            spyOn(document,'getElementById').and.callFake(function (id) {
                var element = {
                    name:id,
                    addEventListener: jasmine.createSpy()
                };
                switch (id){
                    case 'cellSizeInputId':
                        element.value = 4;
                        break;
                    case 'columnsInputId':
                        element.value = 100;
                        break;
                    case 'rowsInputId':
                        element.value = 50;
                        break;
                    case 'canvasId':
                        element.height = -1;
                        element.width = -1;
                        break;
                }
                return element;
            });

        });

        it('should set the canvas width to columns multiplied with cell size', function () {
            this.gridDisplay = getGridDisplay(this.elementIds);

            this.gridDisplay.updateCanvasSize();

            expect(this.gridDisplay.canvas.width).toEqual(400);
        });
        it('should set the canvas height to rows multiplied with cell size', function () {
            this.gridDisplay = getGridDisplay(this.elementIds);

            this.gridDisplay.updateCanvasSize();

            expect(this.gridDisplay.canvas.height).toEqual(200);
        });
    });
    describe('when clicking on the canvas', function () {
        beforeEach(function () {
            spyOn(document,'getElementById').and.callFake(function (id) {
                var element = {
                    name:id,
                    addEventListener: jasmine.createSpy()
                };
                switch (id){
                    case 'cellSizeInputId':
                        element.value = 4;
                        break;
                    case 'columnsInputId':
                        element.value = 100;
                        break;
                    case 'rowsInputId':
                        element.value = 50;
                        break;
                    case 'canvasId':
                        element = document.createElement("CANVAS");
                        element.name = id;
                        element.height = 200;
                        element.width = 400;
                        spyOn(element,'getBoundingClientRect').and.callFake(function () {
                            return {left:5,top:10};
                        });
                        break;
                }
                return element;
            });
        });
        describe('and no cell exists where clicked', function () {
            it('should add cell at location clicked and redraw', function () {
                this.gridDisplay = getGridDisplay(this.elementIds);
                var event = new MouseEvent('mousedown', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                    clientX: 23,
                    clientY: 33
                });
                spyOn(this.gridDisplay,'drawPopulation');

                this.gridDisplay.canvas.dispatchEvent(event);
                var cells = this.gridDisplay.population.getAllCells();
                expect(cells.length).toEqual(1);
                expect(cells[0].x).toEqual(4);
                expect(cells[0].y).toEqual(5);
                expect(this.gridDisplay.drawPopulation).toHaveBeenCalled();
            });
        });
        describe('and a cell exists where clicked', function () {
            it('should remove cell at location clicked', function () {
                this.gridDisplay = getGridDisplay(this.elementIds);
                var event = new MouseEvent('mousedown', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                    clientX: 23,
                    clientY: 33
                });

                spyOn(this.gridDisplay,'drawPopulation');
                this.gridDisplay.population.addCell(createCell(4,5));
                this.gridDisplay.canvas.dispatchEvent(event);
                var cells = this.gridDisplay.population.getAllCells();
                expect(cells.length).toEqual(0);
                expect(this.gridDisplay.drawPopulation).toHaveBeenCalled();
            });
        });
    });
    describe('when drawing population', function () {
        beforeEach(function () {
            var context = {
                clearRect: jasmine.createSpy(),
                fillRect: jasmine.createSpy()
            };
            spyOn(document,'getElementById').and.callFake(function (id) {
                var element = {
                    name:id,
                    addEventListener: jasmine.createSpy()
                };
                switch (id){
                    case 'cellSizeInputId':
                        element.value = 4;
                        break;
                    case 'canvasId':
                        element.name = id;
                        element.height = 200;
                        element.width = 400;
                        element.getContext = jasmine.createSpy().and.returnValue(context);
                        break;
                }
                return element;
            });
            this.context = context;
        });
        it('should get the 2d drawing context and clear existing drawings', function () {
            var gridDisplay = getGridDisplay(this.elementIds);
            gridDisplay.drawPopulation();
            expect(gridDisplay.canvas.getContext).toHaveBeenCalledWith('2d');
            expect(this.context.clearRect).toHaveBeenCalledWith(0,0,
                gridDisplay.canvas.width,
                gridDisplay.canvas.height);
        });
        it('should draw cells of the specified size at each location in population', function () {
            var gridDisplay = getGridDisplay(this.elementIds);
            gridDisplay.population.addCell(createCell(2,3));
            gridDisplay.population.addCell(createCell(3,2));
            gridDisplay.population.addCell(createCell(9,6));

            gridDisplay.drawPopulation();

            expect(this.context.fillRect).toHaveBeenCalledWith(8,12,4,4);
            expect(this.context.fillRect).toHaveBeenCalledWith(12,8,4,4);
            expect(this.context.fillRect).toHaveBeenCalledWith(36,24,4,4);
        });
    });
    describe('when running through generations', function () {
        beforeEach(function () {
            spyOn(document,'getElementById').and.callFake(function (id) {
                var element = {
                    name:id,
                    addEventListener: jasmine.createSpy()
                };
                switch (id){
                    case 'maxGenerationsId':
                        element.value = 1000;
                        break;
                    case 'currentGenerationId':
                        element.value = 0;
                        break;
                }
                return element;
            });
        });
        it('should advance and redraw the populations generations until maximum generation is met',function () {
            var gridDisplay = getGridDisplay(this.elementIds);

            spyOn(window, 'setTimeout').and.callFake(function (action) {
                action();
            });
            spyOn(gridDisplay.population,'advanceGeneration');
            spyOn(gridDisplay,'drawPopulation');

            gridDisplay.runGenerations();

            expect(window.setTimeout).toHaveBeenCalledWith(jasmine.any(Function),500);
            expect(window.setTimeout).toHaveBeenCalledTimes(999);
            expect(gridDisplay.population.advanceGeneration).toHaveBeenCalledTimes(1000);
            expect(gridDisplay.drawPopulation).toHaveBeenCalledTimes(1000);


        });
    });
});