var getGridDisplay = function (elementIds) {
    'use strict';

    var updateCellPopulation = function (event) {
        var cellSize = that.inputs.cellSize.value;
        var canvasBounds = that.canvas.getBoundingClientRect();
        var adjustedX = event.pageX-canvasBounds.left;
        var adjustedY = event.pageY-canvasBounds.top;
        var locationX = Math.floor( adjustedX/cellSize);
        var locationY = Math.floor( adjustedY/cellSize);

        var cell = that.population.getCellByPosition(locationX,locationY);
        if(cell){
            that.population.removeCell(cell);
        }else{
            that.population.addCell(createCell(locationX,locationY));
        }
        that.drawPopulation();
    };

    var that = {
        population: createPopulation(),
        canvas: document.getElementById(elementIds.canvasId),
        inputs:{
            columns: document.getElementById(elementIds.columnsInputId),
            rows: document.getElementById(elementIds.rowsInputId),
            cellSize: document.getElementById(elementIds.cellSizeInputId),
            maxGenerations:document.getElementById(elementIds.maxGenerationsId)
        },
        actions:{
            run: document.getElementById(elementIds.runButtonId)
        },
        updateCanvasSize: function () {
            var width = that.inputs.columns.value * that.inputs.cellSize.value;
            var height = that.inputs.rows.value * that.inputs.cellSize.value;
            that.canvas.width = width;
            that.canvas.height = height;
        },
        drawPopulation: function () {
            var context = that.canvas.getContext('2d');
            context.clearRect(0,0,that.canvas.width,that.canvas.height);
            var cellSize = that.inputs.cellSize.value;
            that.population.getAllCells().forEach(function (cell) {
                context.fillRect(cell.x * cellSize, cell.y * cellSize, cellSize,cellSize);
            });
        },
        runGenerations: function () {
            var currentGeneration = 0;
            var maxGenerations = that.inputs.maxGenerations.value;
            var advanceGenerations = function () {
                that.population.advanceGeneration();
                that.drawPopulation();
                currentGeneration++;
                if(currentGeneration<maxGenerations){
                    setTimeout(advanceGenerations,500);
                }
            };
            advanceGenerations();
        }
    };
    that.inputs.columns.addEventListener('input',that.updateCanvasSize);
    that.inputs.rows.addEventListener('input',that.updateCanvasSize);
    that.inputs.cellSize.addEventListener('input',that.updateCanvasSize);
    that.canvas.addEventListener('mousedown',updateCellPopulation);
    that.actions.run.addEventListener('click', that.runGenerations);

    return that;
};