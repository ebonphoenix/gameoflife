var populationActionFactory = function () {
    'use strict';
    return {
        cellBorn: function (x,y) {
            return {
                executeAction:function (population) {
                    var cell = createCell(x,y);
                    population.addCell(cell);
                }
            };
        },
        cellDies: function (cell) {
            return {
                executeAction:function(population){
                    population.removeCell(cell);
                }
            };
        },
        cellLives: function () {
            return {
                executeAction:function(){
                }
            };
        }
    };
}();