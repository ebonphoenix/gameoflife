var createPopulation = function(){
    'use strict';
    var populationMap = {};
    var getKey = function (x, y) {
        return 'x' + x + '_y' + y;
    };
    var populateBirthActions = function (actions) {
        var potentialBirths = {};
        that.getAllCells().forEach(function (cell) {
            cell.neighboringPositions.forEach(function (neighbor) {
                var neighborKey = getKey(neighbor.x,neighbor.y);
                if(!populationMap[neighborKey]){
                    if(potentialBirths[neighborKey]){
                        potentialBirths[neighborKey].neighboringCells.push(cell);
                    }else{
                        potentialBirths[neighborKey]= {
                            x: neighbor.x, y:neighbor.y,
                            neighboringCells: [cell]};
                    }
                }
            });
        });
        Object.values( potentialBirths ).forEach(function (potentialBirth) {
            if(potentialBirth.neighboringCells.length===3){
                actions.push(populationActionFactory.cellBorn(potentialBirth.x,potentialBirth.y));
            }
        });

    };

    var that = {
        addCell: function (cell) {
            if(!that.getCellByPosition(cell.x,cell.y)) {
                populationMap[getKey(cell.x, cell.y)] = cell;
            }
        },
        removeCell: function (cell) {
            delete populationMap[getKey(cell.x,cell.y)];
        },
        getCellByPosition: function (x, y) {
            return populationMap[getKey(x,y)];
        },
        countCellsAtPositions: function (positions) {
            var matches = 0;
            positions.forEach(function (position) {
                if(that.getCellByPosition(position.x,position.y)){
                    matches++;
                }
            });

            return matches;
        },
        getAllCells: function () {
            return Object.values(populationMap);
        },
        advanceGeneration: function () {
            var actions = [];
            that.getAllCells().forEach(function (cell) {
                actions.push( cell.advanceGeneration(that));
            });

            populateBirthActions(actions);

            actions.forEach(function (action) {
                action.executeAction(that);
            });
        }
    };
    return that;
};