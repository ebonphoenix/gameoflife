var createCell = function (x,y) {
    'use strict';
    var getNeighboringPositions = function () {
        var positions = [];
        for(var i=x-1;i<=x+1;i++){
            for (var j=y-1;j<=y+1;j++){
                if(i===x && j===y){
                    continue;
                }
                positions.push({x:i,y:j});
            }
        }
        return positions;
    };

    var that = {
        x:x,
        y:y,
        neighboringPositions: getNeighboringPositions(),
        advanceGeneration: function (population) {
            var neighborsCount = population.countCellsAtPositions(that.neighboringPositions);
            var action;
            if(neighborsCount < 2 || neighborsCount > 3) {
                action = populationActionFactory.cellDies(that);
            }
            else{
                action = populationActionFactory.cellLives(that);
            }
            return action;
        }
    };
    return that;
};