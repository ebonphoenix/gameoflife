# Game Of Life Kata

The given challenge was to create 
[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) 
in a language of my choosing. I chose to use HTML5 Canvas and 
vanilla JavaScript with Jasmine tests to simplify the 
environment setup and make displaying the output relatively 
simple.

As noted in the instructions, there are two sides of this
problem: the display, and the calculation of
how the cells change as their generations advance.

## It develops...
I decided to start with defining the cells, since the rules for that
seemed pretty straightforward and easy to define.  I
quickly ran into my first challenge: As it turns out, the
cells are not particularly interesting without the greater
context of the overall population.

### Lets start with a cell
So, what defines a cell in the context of this game... turns out the
only unique thing about these cells is their position on the board.
I started with a couple simple tests that specified that the 
constructor arguments would be properties of the cell class. I was
then planning to move into implementing the generations logic, but
quickly realized I could not do that without a greater context.

### It's all about the context
I shifted my focus to the population, which would need to be
some form of managed collection. After some thought I realized
that tracking every possible cell, whether living or dead would
not be a good path, as the board is potentially infinite (or at 
as close to infinite as computers will allow). It made the most
sense to only store living cells. I needed a way to add and remove
cells from the population to simulate the birth of a new cell and
the death of an existing cell. This also required a way to look up
an individual cell.

#### Adding a cell
I started with adding the cell, and quickly determined that I would
need a way to be sure that the cell was in fact added to the 
collection. The easiest way to do this was to introduce the ability 
to return all cells. As  this was the first approach, I stuck with a 
simple array to track the cells added. I knew this would not be the 
long term approach, but it was good enough based on my tests.

#### Finding a cell
When implementing adding a cell, I started to consider what would 
happen if the same cell were added to the population twice. This
encouraged me to start thinking about how to see if a particular cell
is in the collection. This would be useful for many uses within the 
program, so now was a good time to implement it. 

The only thing unique about each cells is its coordinates. Simple 
enough... I altered my test to specify that
when I had a population with several members, I could retrieve a 
specific member based on its coordinates. When digging into how to 
implement it, the least complex way (and most performant) was to use 
an object as a hash. I'd need to generate a key I could use, which 
would need to be a string. That was simple enough to build using the 
coordinates. In short order I was able to return any cell from the
population, and could protect adds from getting muddied with multiple
instances of the same cell.

#### Removing a cell
While there was no driving need to add remove functionality to the
population collection yet, I knew I would need it shortly, and I had
all the functionality in place to wire it up... it also completed
the basic functionality of the population collection. I knocked it
out quickly and moved on to the next challenge.

### Nosey Neighbor
I now had my context, but the cells still had no idea who their
neighbors were. Also, to do the calculations to advance the generation
I needed to get a count of living neighbor cells... so where to start....

#### Count the living
The population collection had all the information to determine if a 
of cells were alive or not... though to be useful, I'd only need to 
look at a subset of the cells in the total population. A method that
takes a set of coordinates, and gives a count of how many are alive
would return the info needed for the cell to determine its
behavior when the generation advanced. Most of the functionality was
in place on the population collection, so the wire-up was pretty simple.

#### Checking out the neighborhood
The cells themselves had the best understanding of their position
and the positions around them, in fact when the cell is created it 
has all the information needed to determine the coordinates of all 
of its neighbors, and this does not change during the life of the cell. 
On construction the cell would now figure out the coordinates of each
of its potential neighbors, and store them for future use.

### Let the generations begin
I now had all the building blocks needed to handle the generations...
or so I thought. I had a suspicion that birth of a new cell might be
a challenge, but I had some rules I could implement,
and could address birth after I had them wired up.

#### Death of a lonely cell
First simplest case... what happens when there's no neighbors? The
cell dies. I was able to write the test and the functionality easily
with what was already in place. Advance Generation always would
remove the cell from the population.

#### It's good to have company
Ok, so it was not desirable that advance generation would always kill 
the cell... instead when there are two or more neighbors, the cell 
should live. Adding these tests required me to introduce logic so 
only lonely cells die (aka are removed from the population). For 
good measure, I added another lonely cell death test for cells with 
only one neighbor. 

#### But not too much company
According to the rules around these cells. If they have more than 3
neighbors, they die... so I needed to add more logic. At this point
specifying a cell with 4 neighbors was pretty trivial, so I was able
to quickly add the functionality to have cells die from 
overpopulation. I also added an upper bounds (8) overpopulation test 
to help show more than 4 neighbors will also die.

#### Effecting the population
I now had cells that would die or survive based on how many 
neighbors they had, but they were only acting as individual cells. I
needed to have the entire population's generation advance together. 
I thought it simple enough... I just need to loop thorough the cells
and advance each of their generations.

#### Wait for it...
While wiring up the advancing the generation of the population, it
occurred to me, that how I had coded it would have each cell living
or dieing in sequence, and that could effect the following cells in
sequence. All cells needed to base their generation calculations off
of the last state of the population. I needed to defer execution.

This pattern of this problem matches pretty closely to the Command 
Pattern, and it seemed like it would solve this challenge handily. 
The two actions I was working with at this time were the cell dies, 
or the cell  survives. I was able to alter the cell's generation 
advancement to return the appropriate action based on its logic, and 
then execute the actions after all have been collected.

#### 3 out of 4 rules isn't good enough
So, I had all the cells in the population dieing or surviving when 
they needed to, but what about births? I had a little bit of a
problem due to my approach.... I was not tracking dead cells, so how
could I tell if a dead cell had 3 neighbors? This took some pondering.
Writing the test was simple... I added cells to the population that
matched the birth criteria, and expected new cells to be created
when the generation advanced, but how was I to implement it?

After some thought, it occurred to me that each living cell knows
its potential neighbors. I would simply need to match up the dead
neighbors of the living cells, if there were exactly three, I would
need to create a new cell at that location. To do this, I would
iterate through the entire population of cells, and track the dead 
neighbors of each cell. Each dead neighbor would have its 
coordinates logged, and each cell that had that neighbor would have 
its coordinates associated with the dead neighbor log.

Once the dead neighbors were logged, each of those records can be 
iterated through, and if the dead neighbors had exactly three
neighboring live cells, an action to add a new cell would be added to
the actions queue.

### Out of the abstract
With the logic in place, it was time to move on to the display. The
Agilist in me was wondering at this point, if I should have instead
started with the display, because that would have been better at a
demo... the pragmatist in me, told that voice to simmer down, the
tests would have sufficed at the demo, and besides it was now water
under the bridge.

#### What do we show
In hindsight, it's clear I overdid it with the controls... for some
reason I got it in my head that I'd want to adjust the size of the drawing
area and the size of the cells when fixed sizes would have been
sufficient. I had also provided several action buttons, that I later
realized were extraneous. In any event, a basic website was needed,
with a canvas to draw upon, and a way to kick off the generations.
The various elements that would need to be interacted with would need
to be known by the javascript, so needed to be passed in to the grid
display class.

#### Event management
The first few events I wired up had to do with sizing the grid. To
not get bogged down in my tangent, I'll shift my focus onto the work
for the core business value... wiring up clicks on the canvas.

For the game of life to be interesting at all, there needs to be some
way specify a starting population. To do that, the canvas needs to
respond to mouse clicks. To test this, I needed to move into some
sophisticated mocking that would simulate a mouse click so it could
be processed correctly. After that, determining if the click should
add or remove the cell was pretty simple. If it does not exist, add
it. If it does, remove it.

#### Still not seeing anything
My tests were telling me that the logic was working... but the canvas
was still blank... but of course it was, I never drew on it in the
first place. I considered various approaches to drawing... should I
draw and remove each cell as part of an action, or should I handle it
another way? I came to the conclusion: it would be best to clear 
and redraw the board based on the population. This would allow me to 
quickly and easily have standard logic that would always draw the 
population, and not muddy the actions of the cells with display logic. 

By taking this route, I simply needed to draw the population after
each mouse action. Sure, some cells would be redrawn multiple times,
but it is quick and simple, and requires much less logic than the 
alternatives.

#### That's not where I clicked!?!?
I got things to the point where the canvas was drawing cells based on
mouse clicks... but unfortunately not where my pointer was. This lead
me down a path of where the click really occurred that can be
complicated in JavaScript. I won't go into the details, but with a
few tweaks, it was fixed.

#### Off to the races
We were now ready to start the generations running. When I started
writing the next test to run through the generations, I realized
that writing a test for a potentially infinite loop did not feel 
right. In fact, I'm not a huge fan of infinite loops in general these
days. I added a new control, to allow a maximum number of 
generations. This made things easier to test, and made me more
confident I would not do bad things on the client's computer. Once
that was in place, it was easy to add the logic that would advance
the generation of the population, and redraw it to the canvas.

Once it came to wiring up the action buttons, I realized I had jumped
the gun and added more than I needed. I stripped away the unneeded 
buttons, and made the run button trigger the newly created method
to run through the generations.

#### Woah speed racer
When I ran the game, the results were not exciting... there was
either no change or only one change depending on the starting 
population. I thought it likely this would happen, but now I had 
confirmation: the generations were advancing too fast so I could not
see them change.

I needed to introduce some form of timing mechanism. Lucky for me,
that's built in to JavaScript. I changed my loop to a recursion of
advancing the generation every half second, and things worked as I
had hoped. This required mocking the built in delay method, to make
the tests not have the same delay.

## Conclusion
This was a fun exercise with a lot of hidden complexity. Following
good TDD practices helped me navigate them with ease. Ever since I
first saw the game of life (and read about it in a Sci-Fi book), I 
had thought it would be fun to program... but I never took the time
to do it. Thanks for encouraging me to do so. I did see some of the
classic challenges of being a solitary developer (not in a pair),
such as going too far down the wrong path, and thus getting dinged by
YAGNI. 
### Potential Changes
As always when coding anything, there's some changes I'd make
were I to spend more time on this.

+   **Refactor Run Generations:** Most of the logic of running the 
generations should be handled by a methon of the population object. 
The drawing  aspect should be a call back passed in.

+   **Grid Display test scope:** some of the tests around the grid 
display use real population and cell objects. From a purist 
perspective  these should be mocked, however I felt at the time, the 
overhead of doing so outweighed the need for purity.

+   **Action button interactions:** Run button should be replaced 
with a stop button while the generations are running, and stop 
should interrupt the generation loop then be replaced with the run 
button.

+   **Display redraw on resize:** Resizing should automatically trigger a redraw.

+   **Additional controls:** the ability to clear the board, or to reset to
pre-run state would be nice. Perhaps the ability to advance one 
generation at a time, and/or change the timing of the interval.
